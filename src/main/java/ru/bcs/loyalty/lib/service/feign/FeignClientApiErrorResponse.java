package ru.bcs.loyalty.lib.service.feign;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class FeignClientApiErrorResponse {
    private String message;
    @JsonProperty("localized_message")
    private String localizedMessage;
    @JsonProperty("class_name")
    private String className;
    private int code;
}
