package ru.bcs.loyalty.lib.service;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.UUID;

@Component
@Slf4j
public class RequestInterceptor
        extends HandlerInterceptorAdapter {

    @Override
    public boolean preHandle(
            HttpServletRequest request,
            HttpServletResponse response,
            Object handler) {
        if (!"OPTIONS".equalsIgnoreCase(request.getMethod()) && !("GET".equalsIgnoreCase(request.getMethod()) && ("/" + RouteMap.PING).equals(request.getRequestURI()))) {
            MDC.put("request_guid", UUID.randomUUID().toString());
            log.info("Request: {} {}", request.getMethod(), request.getRequestURI());
        }
        return true;
    }
}
