package ru.bcs.loyalty.lib.service.modelmapper;

import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import ru.bcs.loyalty.lib.service.contracts.Response;
import ru.bcs.loyalty.lib.service.response.PageResponse;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class SourceDependentModelMapper extends ModelMapper {

    @Override
    public <D> D map(Object source, Class<D> destinationType) {
        Object tmpSource = source;
        if (source == null) {

            if (Response.class.isAssignableFrom(destinationType)) {
                return null;
            }

            tmpSource = new Object();
        }

        return super.map(tmpSource, destinationType);
    }

    public <D, T> List<D> mapList(Collection<T> source, Class<D> destinationItemType) {
        return source.stream()
                .map(entity -> map(entity, destinationItemType))
                .collect(Collectors.toList());
    }

    public <D,T> PageResponse<D> mapPage(Page<T> source, Class<D> destinationItemType) {
        PageResponse<D> pageResponse = new PageResponse<D>();
        pageResponse.setEmpty(source.isEmpty());
        pageResponse.setFirst(source.isFirst());
        pageResponse.setLast(source.isLast());
        pageResponse.setNumber(source.getNumber());
        pageResponse.setSize(source.getSize());
        pageResponse.setTotalElements(source.getTotalElements());
        pageResponse.setTotalPages(source.getTotalPages());
        pageResponse.setContent(mapList(source.getContent(), destinationItemType));
        return pageResponse;
    }
}