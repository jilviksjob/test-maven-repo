package ru.bcs.loyalty.lib.service.annotation.validation.comparefields;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = {CompareFieldsValidator.class})
public @interface CompareFields {

    String message() default "{Object not valid by ru.bcs.loyalty.lib.service.annotation.validation.comparefields.CompareFields}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    String field();

    String compareToField();

    String result();
}
