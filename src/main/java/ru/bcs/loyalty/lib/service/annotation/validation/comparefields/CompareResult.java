package ru.bcs.loyalty.lib.service.annotation.validation.comparefields;

public enum CompareResult {

    GREATER(">"),
    GREATER_OR_EQUAL(">="),
    EQUAL("="),
    LESS("<"),
    LESS_OR_EQUAL("<=");


    private final String value;

    CompareResult(final String value) {
        this.value = value;
    }

    public static CompareResult getByValue(String value) {
        for (CompareResult e : values()) {
            if (e.value.equals(value)) return e;
        }
        return null;
    }

    public String getValue() {
        return value;
    }

}
