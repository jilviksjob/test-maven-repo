package ru.bcs.loyalty.lib.service.params;

import lombok.Data;
import ru.bcs.loyalty.lib.service.contracts.Params;

@Data
public class PageableParams implements Params {
    private Integer pageSize;

    private Integer pageNumber;
}
