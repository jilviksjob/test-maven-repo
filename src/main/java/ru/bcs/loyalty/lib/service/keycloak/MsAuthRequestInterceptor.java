package ru.bcs.loyalty.lib.service.keycloak;


import feign.RequestInterceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;


@Profile({"keycloak"})
@Component
public class MsAuthRequestInterceptor {

    private static final String AUTHORIZATION_HEADER = "Authorization";

    @Autowired
    MsAuthKeycloakTokenService msAuthKeycloakTokenService;

    @Bean
    RequestInterceptor msAuthFeignRequestInterceptor() {
        return template -> {
            /*
            if (template.url().contains("/sys/send.php")) {
                //don't add auth header for the specific url
                return;
            }
            */
            template.header(AUTHORIZATION_HEADER,
                    "Bearer " + msAuthKeycloakTokenService.getAccessToken().getToken());
        };
    }
}
