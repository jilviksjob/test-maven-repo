package ru.bcs.loyalty.lib.service.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;


@Data
@Slf4j
public class ApiError {
    private String message;
    @JsonProperty("localized_message")
    private String localizedMessage;
    @JsonProperty("class_name")
    private String className;
    private int code;

    public static ApiError createFromException(Throwable exception) {
        ApiError apiError = new ApiError();
        apiError.setMessage(exception.getMessage());
        apiError.setLocalizedMessage(exception.getLocalizedMessage());
        apiError.setClassName(exception.getClass().getSimpleName());

        return apiError;
    }
}