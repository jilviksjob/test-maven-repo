package ru.bcs.loyalty.lib.service.feign;

import com.fasterxml.jackson.databind.ObjectMapper;
import feign.FeignException;
import feign.Response;
import feign.codec.DecodeException;
import feign.codec.Decoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.ResponseEntityDecoder;
import org.springframework.cloud.openfeign.support.SpringDecoder;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Type;

public class LoyaltyDecoder implements Decoder {
    private Decoder decoder;

    public LoyaltyDecoder() {
        ObjectFactory<HttpMessageConverters> objectFactory = () -> new HttpMessageConverters(new MappingJackson2HttpMessageConverter());
        this.decoder = new ResponseEntityDecoder(new SpringDecoder(objectFactory));
    }

    @Override
    public Object decode(Response response, Type type) throws IOException, DecodeException, FeignException {

        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        org.apache.commons.io.IOUtils.copy(response.body().asInputStream(), byteArrayOutputStream);
        ObjectMapper mapper = new ObjectMapper();
        FeignClientApiResponse clientApiResponse = mapper.readValue(byteArrayOutputStream.toByteArray(), FeignClientApiResponse.class);
        if (clientApiResponse.isError()) {
            FeignClientApiErrorResponse clientApiError = mapper.convertValue(clientApiResponse.getData(), FeignClientApiErrorResponse.class);
            throw new FeignClientApiException(response.request().url(),clientApiError);
        }
        Response copiedResponse = Response.builder()
                .status(response.status())
                .reason(response.reason())
                .headers(response.headers())
                .body(mapper.writeValueAsBytes(clientApiResponse.getData()))
                .request(response.request())
                .build();


        return this.decoder.decode(copiedResponse, type);
    }
}
