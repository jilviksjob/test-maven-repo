package ru.bcs.loyalty.lib.service.api;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

import java.util.Date;

@Data
@Slf4j
public class ApiResponse<T> {
    public enum Status {
        OK, ERROR
    }

    private T data;
    private Date date;
    @JsonProperty("request_guid")
    private String requestGuid;
    private String status;

    public ApiResponse(T data) {
        this(data, Status.OK);
    }

    public ApiResponse(T data, Status status) {
        this.status = ApiResponse.getStatusText(status);
        this.data = data;
        this.date = new Date();
        this.requestGuid = MDC.get("request_guid");
//        log.info(this.toString());
    }

    private static String getStatusText(Status status) {
        if (status == Status.OK) {
            return "ok";
        } else if (status == Status.ERROR) {
            return "error";
        }
        return "undefined";
    }
}
