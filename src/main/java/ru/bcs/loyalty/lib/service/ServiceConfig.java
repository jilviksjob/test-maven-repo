package ru.bcs.loyalty.lib.service;

public class ServiceConfig {
    public static final String dateFormat = "yyyy-MM-dd";
    public static final String timeZone = "UTC";
    public static final String dateTimeFormat = "yyyy-MM-dd HH:mm:ss";
}
