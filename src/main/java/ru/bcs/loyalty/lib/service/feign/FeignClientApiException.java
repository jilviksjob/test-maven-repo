package ru.bcs.loyalty.lib.service.feign;

import feign.FeignException;
import lombok.Getter;

@Getter
public class FeignClientApiException extends FeignException {
    private FeignClientApiErrorResponse error;

    public FeignClientApiException(String url, FeignClientApiErrorResponse feignClientApiErrorResponse) {
        super(0, url + ": " + feignClientApiErrorResponse.getClassName() + "(" + feignClientApiErrorResponse.getCode() + ") - " + feignClientApiErrorResponse.getMessage());
        this.error = feignClientApiErrorResponse;
    }
}
