package ru.bcs.loyalty.lib.service.annotation.validation.comparefields;


import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.lang.reflect.Field;

@Slf4j
public class CompareFieldsValidator implements ConstraintValidator<CompareFields, Object> {

    private String field;
    private String compareToField;
    private String result;

    @Override
    public void initialize(CompareFields constraint) {
        field = constraint.field();
        compareToField = constraint.compareToField();
        result = constraint.result();
    }

    @Override
    public boolean isValid(Object object, ConstraintValidatorContext context) {
        try {
            Comparable fieldValue = (Comparable) getFieldValue(object, field);
            Comparable compareToFieldValue = (Comparable) getFieldValue(object, compareToField);
            CompareResult compareResult = CompareResult.getByValue(result);
            if (fieldValue == null || compareToFieldValue == null) {
                return true;
            }
            return compare(fieldValue, compareToFieldValue, compareResult);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }

    private Boolean compare(Comparable fieldValue, Comparable compareToFieldValue, CompareResult compareResult){
        if (compareResult == CompareResult.LESS) {
            return fieldValue.compareTo(compareToFieldValue) < 0;
        }
        if (compareResult == CompareResult.LESS_OR_EQUAL) {
            return fieldValue.compareTo(compareToFieldValue) <= 0;
        }
        if (compareResult == CompareResult.EQUAL) {
            return fieldValue.compareTo(compareToFieldValue) == 0;
        }
        if (compareResult == CompareResult.GREATER) {
            return fieldValue.compareTo(compareToFieldValue) > 0;
        }
        if (compareResult == CompareResult.GREATER_OR_EQUAL) {
            return fieldValue.compareTo(compareToFieldValue) >= 0;
        }
        throw new RuntimeException("CompareFields annotation \"result\" param " +
                "should be ru.bcs.loyalty.lib.service.annotation.validation.comparefields.CompareResult String");
    }

    private Object getFieldValue(Object object, String fieldName)
            throws NoSuchFieldException, IllegalAccessException {
        Field field = null;

        for (Class<?> c = object.getClass(); c != null; c = c.getSuperclass()) {
            try {
                field = c.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) { }
            if (field != null) {
                field.setAccessible(true);
                return field.get(object);
            }
        }
        throw new NoSuchFieldException(fieldName);
    }

}
