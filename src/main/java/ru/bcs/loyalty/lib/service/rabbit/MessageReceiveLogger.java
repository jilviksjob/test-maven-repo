package ru.bcs.loyalty.lib.service.rabbit;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;
import org.springframework.amqp.AmqpException;
import org.springframework.amqp.core.Correlation;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessagePostProcessor;

import java.util.UUID;

@Slf4j
public class MessageReceiveLogger implements MessagePostProcessor {
    @Override
    public Message postProcessMessage(Message message) throws AmqpException {
        this.log(message);
        return message;
    }

    @Override
    public Message postProcessMessage(Message message, Correlation correlation) {
        this.log(message);
        return message;
    }

    protected void log(Message message) {
        MDC.put("listener_request_guid", UUID.randomUUID().toString());
        log.info("Received from queue \"" + message.getMessageProperties().getConsumerQueue() + "\" message (type: " + message.getMessageProperties().getContentType() + "): " + new String(message.getBody()));
    }
}
