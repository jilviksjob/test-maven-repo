package ru.bcs.loyalty.lib.service;

import org.modelmapper.convention.MatchingStrategies;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.bcs.loyalty.lib.service.modelmapper.SourceDependentModelMapper;

@Configuration
public class ModelMapperConfig {
    @Bean
    public SourceDependentModelMapper modelMapper() {
        SourceDependentModelMapper sourceDependentModelMapper = new SourceDependentModelMapper();
        sourceDependentModelMapper.getConfiguration()
                .setMatchingStrategy(MatchingStrategies.STRICT);
        return sourceDependentModelMapper;
    }
}

