package ru.bcs.loyalty.lib.service.keycloak;

import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.KeycloakBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;


@Service
@Profile({"keycloak"})
public class MsAuthKeycloakTokenService extends KeycloakTokenService {


    @Autowired
    MsAuthKeycloakTokenService(Environment env){

        super(
            KeycloakBuilder.builder()
                    .realm(env.getRequiredProperty("keycloak-token-service.ms-auth.realm"))
                    .serverUrl(env.getRequiredProperty("keycloak-token-service.ms-auth.auth-url"))
                    .clientId(env.getRequiredProperty("keycloak-token-service.ms-auth.client-id"))
                    .username(env.getRequiredProperty("keycloak-token-service.ms-auth.username"))
                    .password(env.getRequiredProperty("keycloak-token-service.ms-auth.password"))
                    .grantType(OAuth2Constants.PASSWORD).build()
        );
    }

}
