package ru.bcs.loyalty.lib.service.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import ru.bcs.loyalty.lib.service.contracts.Response;

import java.util.List;

@Data
public class PageResponse<T> implements Response {
    private List<T> content;
    @JsonProperty("total_elements")
    private long totalElements;
    @JsonProperty("total_pages")
    private long totalPages;
    private int size;
    private int number;
    private boolean empty;
    private boolean first;
    private boolean last;
}
