package ru.bcs.loyalty.lib.service.feign;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Date;

@Data
public class FeignClientApiResponse<T> {
    private T data;
    private Date date;
    @JsonProperty("request_guid")
    private String requestGuid;
    private String status;

    public boolean isError() {
        return !"ok".equals(this.status);
    }
}
