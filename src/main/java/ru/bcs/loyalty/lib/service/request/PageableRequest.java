package ru.bcs.loyalty.lib.service.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import ru.bcs.loyalty.lib.service.contracts.Request;

@Data
public abstract class PageableRequest implements Request {
    @JsonProperty("page_size")
    private Integer pageSize;

    @JsonProperty("page_number")
    private Integer pageNumber;
}
