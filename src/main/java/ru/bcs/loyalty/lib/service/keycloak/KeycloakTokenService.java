package ru.bcs.loyalty.lib.service.keycloak;

import org.keycloak.admin.client.Keycloak;
import org.keycloak.representations.AccessTokenResponse;
import org.springframework.context.annotation.Profile;


@Profile({"keycloak"})
public class KeycloakTokenService {

    private Keycloak keycloak;

    public KeycloakTokenService(Keycloak keycloak) {
        this.keycloak = keycloak;
    }

    public AccessTokenResponse getAccessToken(){
        return keycloak.tokenManager().getAccessToken();
    }

}
