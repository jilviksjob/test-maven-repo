package ru.bcs.loyalty.lib.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;
import ru.bcs.loyalty.lib.service.api.ApiError;
import ru.bcs.loyalty.lib.service.api.ApiResponse;


@Slf4j
@RestControllerAdvice
public class ResponseWrapper implements ResponseBodyAdvice<Object> {
    @Override
    public boolean supports(MethodParameter returnType, Class<? extends HttpMessageConverter<?>> converterType) {
        return true;
    }

    @ExceptionHandler
    public Throwable handle(Throwable ex) {
        log.error(ex.getClass().getSimpleName(), ex);
        return ex;
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType,
                                  Class<? extends HttpMessageConverter<?>> selectedConverterType, ServerHttpRequest request,
                                  ServerHttpResponse response) {
        ApiResponse apiResponse;
        if (!selectedContentType.isCompatibleWith(MediaType.APPLICATION_JSON)) {
            return body;
        }
        if (body instanceof Throwable) {

            apiResponse = new ApiResponse(ApiError.createFromException((Throwable) body), ApiResponse.Status.ERROR);
        } else {
            apiResponse = new ApiResponse(body);
        }
        if (request.getMethod() != HttpMethod.GET || !("/" + RouteMap.PING).equals(request.getURI().getPath())) {
            log.info("Response: {}", apiResponse);
        }
        return apiResponse;
    }
}
